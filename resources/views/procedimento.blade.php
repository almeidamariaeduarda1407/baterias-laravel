@extends('templates.base')

@section('conteudo')
    <main>
        <h1>Procedimento</h1>
        <hr>
        <p>
            Com o auxílio de um multímetro, medimos o valor do resistor e o valor da tensão interna das pilhas/baterias (E). Com o resistor (R) ligado ao multímetro, circulará uma corrente (I) que produzirá queda de tensão interna (Vr) e a queda de tensão externa (VR). Após esses procedimentos medimos a resistência interna de uma pilha ou de uma bateria, através da medição de sua tensão interna (sem carga) "E", da tensão externa (com carga) "VR" e da própria resistência externa "R" e aplicando a equação da resistência interna "r".
        </p>
    </main>
@endsection

@section('rodape')
    <h4>Rodapé da página procedimento</h4>
@endsection